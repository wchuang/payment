<?php
namespace Payment\Charge\Unionpay;

use Payment\Common\Unionpay\Data\Charge\WebChargeData;
use Payment\Common\Unionpay\UnionpayBaseStrategy;


class UnionpayWebCharge extends UnionpayBaseStrategy
{
    /**
     * 获取支付对应的数据完成类
     * @return string
     * @author helei
     */
    protected function getBuildDataClass()
    {
        return WebChargeData::class;
    }
}