<?php
namespace Payment\Common;


use Payment\Utils\ArrayUtil;

require __DIR__ . '\Unionpay\sdk\acp_service.php';
final class UnionpayConfig extends ConfigInterface
{
    // 前台支付网关
    public $webGatewayUrl = SDK_FRONT_TRANS_URL;

    public $backendGateWayUrl = SDK_BACK_TRANS_URL;

    public $appGatewayUrl = SDK_App_Request_Url;


    // 采用的编码
    public $inputCharset = 'utf-8';

    // 证书密钥
    public $cakey;

    // 商户id
    public $merId ;

    //签名证书路径
    public  $signCertPath;

    //验签证书路径;
    public  $verifyCertPath;

    // 用于异步通知的地址
    public $notifyUrl;

    // 用于同步通知的地址
    public $returnUrl;


    // 安全证书的路径
    public $cacertPath;


    // 加密方式 默认使用RSA
    public $signType;



    //银联sdk操作类
    /**
     * @var \AcpService
     */
    public static $workService;

    public function __construct(array $config)
    {
        // 初始化配置信息
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }

    }

    /**
     * 检查传入的配置文件信息是否正确
     * @param array $config
     * @throws PayException
     * @author helei
     */
    private function initConfig(array $config)
    {

        $config = ArrayUtil::paraFilter($config);
        $this->signType = 'RSA';
        // 初始化 加密方式,默认采用RSA
        if (!empty($config['sign_type'])) {
            $this->signType = strtoupper($config['sign_type']);
        }

        self::$workService = new \AcpService();

        // 初始 商户id
        if (array_key_exists('merId', $config) ) {
            $this->merId = $config['merId'];
        } else {
            throw new PayException('请设置商户id');
        }
        
        // 初始 证书密钥
        if (array_key_exists('cakey', $config) ) {
            $this->cakey = $config['cakey'];
        } else {
            throw new PayException('证书密钥');
        }

        // 初始 签名证书路径
        if (array_key_exists('sign_cert_path', $config) ) {
            $this->signCertPath = $config['sign_cert_path'];
            if(!file_exists($this->signCertPath)){
                throw new PayException('签名证书路径 文件不存在');
            }
        } else {
            throw new PayException('签名证书路径 缺失');
        }

        // 初始 验签证书路径;
        if (array_key_exists('verify_cert_path', $config) ) {
            $this->verifyCertPath = $config['verify_cert_path'];
        } else {
            throw new PayException('验签证书路径 缺失');
        }

        // 初始 异步通知的地址
        if (array_key_exists('notify_url', $config) ) {
            $this->notifyUrl = $config['notify_url'];
        } else {
            throw new PayException('异步通知的地址 缺失');
        }

        // 初始 同步通知的地址
        if (array_key_exists('return_url', $config) ) {
            $this->returnUrl = $config['return_url'];
        } else {
            throw new PayException('同步通知的地址 缺失');
        }



    }
}