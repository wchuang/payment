<?php
namespace Payment\Common;


interface BaseStrategy
{
    /**
     * 处理具体的业务
     * @param array $data
     * @return mixed
     * @author helei
     */
    public function handle(array $data);
}