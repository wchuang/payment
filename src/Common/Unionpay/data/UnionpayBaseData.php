<?php


namespace Payment\Common\Unionpay\Data;

use Payment\Common\UnionpayConfig;
use Payment\Common\BaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;
use Payment\Utils\RsaEncrypt;


abstract class UnionpayBaseData extends BaseData
{

    /**
     * @param UnionpayConfig $config
     * @param array $reqData
     * @throws PayException
     */
    public function __construct(UnionpayConfig $config, array $reqData)
    {
        parent::__construct($config, $reqData);

        $this->signType = $config->signType;// 默认使用RSA 进行加密处理
    }

    /**
     * 签名算法实现
     * @param string $signStr
     * @author helei
     */
    protected function makeSign($signStr)
    {
        $work = UnionpayConfig::$workService;
        $work::sign($this->retData);
    }
}