<?php
namespace Payment\Common\Unionpay\Data\Charge;

use Payment\Utils\ArrayUtil;

/**
 * Class WebChargeData
 *
 * @inheritdoc
 *
 * @package Payment\Charge\Unionpay\Data
 */
class WebChargeData extends ChargeBaseData
{
    /**
     * 构建 即时到帐 加密数据
     * @author helei
     */
    public function buildData()
    {
        $signData = array(
            //以下信息非特殊情况不需要改动
            'version' => '5.0.0',                 //版本号
            'encoding' => 'utf-8',				  //编码方式
            'txnType' => '01',				      //交易类型
            'txnSubType' => '01',				  //交易子类
            'bizType' => '000201',				  //业务类型
            'frontUrl' =>  trim($this->returnUrl),  //前台通知地址
            'backUrl' => trim($this->notifyUrl),  //后台通知地址
            'signMethod' => '01',	              //签名方法
            'channelType' => '07',	              //渠道类型，07-PC，08-手机
            'accessType' => '0',		          //接入类型
            'currencyCode' => '156',	          //交易币种，境内商户固定156

            //TODO 以下信息需要填写
            'merId' => trim($this->merId),		//商户代码，请改自己的测试商户号，此处默认取demo演示页面传递的参数
            'orderId' => trim($this->order_no),	//商户订单号，8-32位数字字母，不能含“-”或“_”，此处默认取demo演示页面传递的参数，可以自行定制规则
            'txnTime' => date("YmdHms",time()),	//订单发送时间，格式为YYYYMMDDhhmmss，取北京时间，此处默认取demo演示页面传递的参数
            'txnAmt' => trim($this->amount) * 100,	//交易金额，单位分，此处默认取demo演示页面传递的参数
// 		'reqReserved' =>'透传信息',        //请求方保留域，透传字段，查询、通知、对账文件中均会原样出现，如有需要请启用并修改自己希望透传的数据

            //TODO 其他特殊用法请查看 special_use_purchase.php
        );

        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }
}