<?php
/**
 * @author: helei
 * @createTime: 2016-07-15 09:25
 * @description: 测试支付接口
 */

require_once __DIR__ . '/../autoload.php';

use Payment\ChargeContext;
use Payment\Config;
use Payment\Common\PayException;

date_default_timezone_set('Asia/Shanghai');


//  生成订单号 便于测试
function createPayid()
{
    return date('Ymdhis', time()).substr(floor(microtime()*1000),0,1).rand(0,9);
}

// 订单信息
$payData = [
    "order_no"	=> createPayid(),
    "amount"	=> '0.01',// 单位为元 ,最小为0.01
    "client_ip"	=> '127.0.0.1',
    "subject"	=> '测试支付',
    "body"	=> '支付接口测试',
    "show_url"  => 'http://mall.tiyushe.com/goods/23.html',// 支付宝手机网站支付接口 该参数必须上传 。其他接口忽略
    "extra_param"	=> '',
];

$config = [
    'merId' =>  '826440148990096',
    'cakey' =>  '70611231202',
    'sign_cert_path'    =>  "",
    'verify_cert_path'    =>  '',
    'notify_url'    =>  '',
    'return_url'    =>  '',

];

/**
 * 实例化支付环境类，进行支付创建
 */
$charge = new ChargeContext();

try {
//    $type = Config::UNIONPAY_WEB;
    $type = Config::UNIONPAY_APP;

    $charge->initCharge($type, $config);
    $ret = $charge->charge($payData);

//    echo $ret;

     var_dump($ret);exit;
} catch (PayException $e) {
    echo $e->errorMessage();exit;
}



?>

