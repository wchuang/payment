<?php
/**
 * @author: helei
 * @createTime: 2016-07-25 15:57
 * @description: 支付通知回调
 */

require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/testNotify.php';

use Payment\NotifyContext;
use Payment\Common\PayException;
use Payment\Config;

// 支付宝的配置文件
$aliconfig = require_once __DIR__ . '/aliconfig.php';

// 微信的配置文件
$wxconfig = require_once __DIR__ . '/wxconfig.php';

$config = [
    'merId' =>  '123121231231233123123',
    'cakey' =>  '5123123',
];
$notify = new NotifyContext();

$callback = new TestNotify();
$file = __DIR__ . '\notify_union_data.txt';
$post = file_get_contents($file);
$_POST = json_decode($post,true);

//file_put_contents(time(),json_encode($_POST));
try {
    // 支付宝回调
    //$notify->initNotify(Config::ALI, $aliconfig);

    // 微信回调
    //$notify->initNotify(Config::WEIXIN, $wxconfig);

    $notify->initNotify(Config::UNIONPAY, $config);

    $ret = $notify->notify($callback);
} catch (PayException $e) {
    echo $e->errorMessage();exit;
}

var_dump($ret);exit;